import Vue from 'vue';
import Vuex from 'vuex';

import auth from './modules/auth';
import view from './modules/view';
import cards from './modules/cards';
import user from './modules/user';
import messages from './modules/messages';
import videocall from './modules/videocall';
import lk from './modules/lk';

Vue.use(Vuex);

const store = () => new Vuex.Store({

    modules: {
        auth,
        view,
        cards,
        user,
        messages,
        videocall,
        lk
    }
})
  
export default store