import {db} from '../../../main'
import firebase from 'firebase/app'
require('firebase/auth')
require('firebase/database')
import 'firebase/firestore'

const loadUserData = (state, id) => {
    firebase.firestore().collection('users').where('uid', '==', id).onSnapshot(snapshot => {
        snapshot.forEach(doc => {
            state.commit('setLoadedUserData', doc.data())
        })
    })
}

export default {
    loadUserData
}