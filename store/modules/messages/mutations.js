import Vue from 'vue';
import {db} from '../../../main'
import firebase from 'firebase/app'
require('firebase/auth')
require('firebase/database')
import 'firebase/firestore'

const uploadConversations = (state, data) => {
    
    let convId = data.conversationId

    if (state.conversations[convId] == undefined) {
        let messages = [data.conversationMessage]
        let obj = {
            [convId]: {
                messages: messages, 
                info: data.conversationData
            },
        }
        
        Vue.set(state.conversations, [convId], obj[convId])

    } else {
        // state.conversations[convId].messages = []
        state.conversations[convId].messages.push(data.conversationMessage)
    }
}

const checkChat = (state, id) => {
    state.checkedChat = id
}

const createLocalConversation = (state, data) => {

    let newConversation = {
        [data.conversation]: {
            info: {
                user1: data.myData,
                user2: {
                    uid: data.remoteUser.uid,
                    userName: data.remoteUser.userName,
                    photoURL: data.remoteUser.photoURL
                },
                timestamp: new Date()
            },
            messages: []
        },
    }
    
    Vue.set(state.conversations, [data.conversation], newConversation[data.conversation])
}

const addMessages = (state, data) => {
    let newTime = data.message.timestamp.seconds + (data.message.timestamp.nanoseconds * 0.000000001)
    let currentTime = state.conversations[data.roomId].messages[state.conversations[data.roomId].messages.length - 1].timestamp.seconds + (state.conversations[data.roomId].messages[state.conversations[data.roomId].messages.length - 1].timestamp.nanoseconds * 0.000000001)

    if (state.conversations[data.roomId].messages.length > 0 && 
        newTime > currentTime) {
        state.conversations[data.roomId].messages.push(data.message)
    }
}

const updateUnreadMessages = (state, counter) => {
    state.messages.unread = counter
}

export default {
    uploadConversations,
    checkChat,
    createLocalConversation,
    addMessages,
    updateUnreadMessages
}