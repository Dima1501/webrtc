import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
    conversations: {},
    checkedChat: null,
    messages: {
      unread: null
    }
}

export default {
  namespaced:   true,
  state,
  actions,
  mutations,
  getters
}