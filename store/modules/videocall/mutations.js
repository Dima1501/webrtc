const addCandidate = (state, candidate) => {
    state.callerCandidates.push(candidate)
}

const addCalleeCandidate = (state, candidate) => {
    state.calleeCandidates.push(candidate)
}

const addCall = (state, payload) => {
    state.videocalls[payload.from] = payload
}

const removeCall = (state, payload) => {
    delete state.videocalls[payload.from]
}

export default {
    addCandidate,
    addCall,
    addCalleeCandidate,
    removeCall
}