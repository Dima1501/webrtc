import path from 'path'
import fs from 'fs'

module.exports = {
  server: {
    // https: {
    //   key: fs.readFileSync('./server.key', 'utf8'),
    //   cert: fs.readFileSync('./certificate.crt', 'utf8')
    // },
    port: 3000, // default: 3000
    // host: '194.67.90.118' // default: localhost
  },

  /*
  ** Headers of the page
  */
  head: {
    title: 'callanythere',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  plugins: [
    '~plugins/filters.js'
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

