import Vue from 'vue'
import { firestorePlugin } from 'vuefire'
import firebase from 'firebase/app'
import 'firebase/firestore'

Vue.use(firestorePlugin)
if (!firebase.apps.length) {
  firebase.initializeApp({
    apiKey: "AIzaSyB58gXCJlmeiRKp5aEIMR6oVtrvqcTV68I",
    authDomain: "callthere-721fc.firebaseapp.com",
    databaseURL: "https://callthere-721fc.firebaseio.com",
    projectId: "callthere-721fc",
    storageBucket: "callthere-721fc.appspot.com",
    messagingSenderId: "918726371698",
    appId: "1:918726371698:web:96122eb70876c4209655eb"
  })
}

export const db = firebase.firestore()
